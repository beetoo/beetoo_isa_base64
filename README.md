# BeeToo ISA Base64 encode & decode functions

## Description

Allow to encode strings, arrays into base64 string.
And decode base64 encoded string.

## Installation

*If you are using npm: add module dependency into your package.json:*

```
  "dependencies": {
	"beetoo-isa-base64": "git+https://bitbucket.org/beetoo/beetoo_isa_base64#master"
  }
```

*Or: **just copy and paste content of index.js into your project's scripts***

## Usage

*Take a lool at iRidium's example project placed in directory: /resource/base64_demo.irpz*
```
IR.Base64.fromString('foobar'); //returns string 'Zm9vYmFy'
IR.Base64.fromByteArray([102, 111, 111, 98, 97, 114]); //returns string 'Zm9vYmFy'
IR.Base64.b64ToByteArray('Zm9vYmFy'); //returns array [102, 111, 111, 98, 97, 114]
IR.Base64.b64ToString('Zm9vYmFy'); //returns string 'foobar'

IR.Base64.strToBase64('foobar'); //the same as fromString
IR.Base64.uint8ToBase64([102, 111, 111, 98, 97, 114]); //the same as fromByteArray

//Or, if you have defined globally BeeToo object, than:
BeeToo.Base64.fromString('foobar');  //returns string 'Zm9vYmFy'
BeeToo.Base64.fromByteArray([102, 111, 111, 98, 97, 114]); //returns string 'Zm9vYmFy'
BeeToo.Base64.b64ToByteArray('Zm9vYmFy'); //returns array [102, 111, 111, 98, 97, 114]
BeeToo.Base64.b64ToString('Zm9vYmFy'); //returns string 'foobar'

BeeToo.Base64.strToBase64('foobar'); //the same as fromString
BeeToo.Base64.uint8ToBase64([102, 111, 111, 98, 97, 114]); //the same as fromByteArray
```
## Copyrights

BeeToo, Alexander Borovsky, 2015.

Current library based on https://github.com/beatgammit/base64-js by [beatgammit](https://github.com/beatgammit)

## Licence

**Published under MIT license**

* http://opensource.org/licenses/MIT

* https://ru.wikipedia.org/wiki/%D0%9B%D0%B8%D1%86%D0%B5%D0%BD%D0%B7%D0%B8%D1%8F_MIT